import React, { useState, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Alert,
  Pressable,
} from "react-native";
import axios from "axios";
import { URL } from "../config";

export default function Login(props) {
  const [form, setForm] = useState({
    email: "",
    password: "",
  });

  // We're using this reference to press next and go to next input instead of submitting
  const passwordRef = useRef();

  const handleChange = (text, input) => {
    setForm({ ...form, [input]: text });
  };

  const handleSubmit = async () => {
    try {
      const res = await axios.post(`${URL}/users/login`, {
        email: form.email,
        password: form.password,
      });
      if (res.data.ok) {
        props.logIn(res.data.token);
      } else {
        Alert.alert(
          res.data.message,
          "", // <- this part is optional, you can pass an empty string
          [{ text: "OK" }],
          { cancelable: false }
        );
      }
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.welcome}>Welcome!</Text>
      <Text style={styles.text}>Email:</Text>
      <TextInput
        placeholder="Email"
        style={styles.input}
        onChangeText={(text) => handleChange(text.toLowerCase(), "email")}
        value={form.email}
        returnKeyType="next"
        onSubmitEditing={() => {
          passwordRef.current.focus();
        }}
      />
      <Text style={styles.text}>Password:</Text>
      <TextInput
        placeholder="Password"
        style={styles.input}
        onChangeText={(text) => handleChange(text, "password")}
        value={form.password}
        onSubmitEditing={handleSubmit}
        ref={passwordRef}
        secureTextEntry={true}
      />
      <Button title="Log in" onPress={() => handleSubmit()} />
      <Pressable onPress={() => props.setHasAccount(false)}>
        <Text style={styles.underline}>I don't have an account</Text>
      </Pressable>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: 250,
    width: "100%",
  },
  welcome: {
    marginBottom: 25,
    fontSize: 25,
    color: "white",
  },
  text: {
    marginBottom: 5,
    color: "white",
  },
  input: {
    height: 40,
    borderWidth: 1,
    padding: 10,
    width: "80%",
    marginBottom: 20,
    color: "white",
  },
  underline: {
    textDecorationLine: "underline",
  },
});
