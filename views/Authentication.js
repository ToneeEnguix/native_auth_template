import React, { useState } from "react";
import Login from "../components/Login";
import Register from "../components/Register";

export default function Authentication(props) {
  const [hasAccount, setHasAccount] = useState(true);

  if (hasAccount) {
    return <Login {...props} setHasAccount={(bool) => setHasAccount(bool)} />;
  } else {
    return (
      <Register {...props} setHasAccount={(bool) => setHasAccount(bool)} />
    );
  }
}
