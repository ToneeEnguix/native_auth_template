import React from "react";
import { Text, View, Pressable, StyleSheet } from "react-native";

export default function Authentication(props) {
  // you can use this part to add routing between screens like:
  // - home
  // - profile
  // - settings
  // - etc

  return (
    <View style={styles.container}>
      <Text>{props.user?.email}</Text>
      <Text>You are logged in</Text>
      <Pressable onPress={() => props.logOut()}>
        <Text>Log out</Text>
      </Pressable>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    marginTop: 250,
    width: "100%",
  },
});
