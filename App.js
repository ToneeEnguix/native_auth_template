import { StatusBar } from "expo-status-bar";
import React, { useState, useEffect } from "react";
import {
  ImageBackground,
  StyleSheet,
  View,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import Router from "./views/Router";
import Authentication from "./views/Authentication";
import AsyncStorage from "@react-native-async-storage/async-storage";
import axios from "axios";
import { URL } from "./config";

export default function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [user, setUser] = useState(null);

  const image = {
    uri:
      "https://images.unsplash.com/photo-1569982175971-d92b01cf8694?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80",
  };

  useEffect(() => {
    const getToken = async () => {
      try {
        const jsonValue = await AsyncStorage.getItem("token");
        const token = jsonValue != null ? JSON.parse(jsonValue) : null;
        if (token) logIn(token);
      } catch (err) {
        console.info(err);
      }
    };
    getToken();
  }, []);

  const logIn = async (token) => {
    axios.defaults.headers.common["Authorization"] = token;
    try {
      const res = await axios.get(`${URL}/users/verify_token`);
      await AsyncStorage.setItem("token", JSON.stringify(token));
      setIsLoggedIn(true);
      setUser(res.data.user);
    } catch (err) {
      console.info(err);
    }
  };

  const logOut = async () => {
    try {
      await AsyncStorage.removeItem("token");
      setIsLoggedIn(false);
    } catch (err) {
      console.info(err);
    }
  };

  return (
    <View style={styles.main}>
      {/* This touchable without feedback will make keyboard disappear when touched */}
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <ImageBackground source={image} resizeMode="cover" style={styles.image}>
          <StatusBar style="auto" />
          {isLoggedIn ? (
            <Router user={user} logOut={logOut} />
          ) : (
            <Authentication logIn={(token) => logIn(token)} />
          )}
        </ImageBackground>
      </TouchableWithoutFeedback>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  image: {
    flex: 1,
    justifyContent: "center",
    width: "100%",
  },
});
