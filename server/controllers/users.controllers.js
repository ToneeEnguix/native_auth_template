const User = require("../models/users.models");
const argon2 = require("argon2");
// https://github.com/ranisalt/node-argon2/wiki/Options
const jwt = require("jsonwebtoken");
const validator = require("validator");
const jwt_secret = process.env.JWT_SECRET;

const register = async (req, res) => {
  const { email, password, password2 } = req.body;
  if (!email || !password || !password2)
    return res.status(200).send({ ok: false, message: "All fields required" });
  if (password !== password2) {
    return res.status(200).send({ ok: false, message: "Passwords must match" });
  }
  if (!validator.isEmail(email))
    return res
      .status(200)
      .send({ ok: false, message: "Email address invalid" });
  try {
    const user = await User.findOne({ email });
    if (user)
      return res.status(200).send({ ok: false, message: "Invalid data" });
    const hash = await argon2.hash(password);
    const newUser = {
      email,
      password: hash,
    };
    new User(newUser).save();
    const token = jwt.sign(newUser, jwt_secret, {
      expiresIn: "1h",
    }); //{ expiresIn: '365d' }
    res
      .status(200)
      .send({ ok: true, message: "Successfully registered", token });
  } catch (error) {
    res.status(400).send({ ok: false, error });
  }
};

const login = async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password)
    return res.status(200).send({ ok: false, message: "All fields required" });
  if (!validator.isEmail(email))
    return res.status(200).send({ ok: false, message: "Invalid data" });
  try {
    const user = await User.findOne({ email });
    if (!user)
      return res.status(200).send({ ok: false, message: "Invalid data" });
    const match = await argon2.verify(user.password, password);
    if (match) {
      const token = jwt.sign(user.toJSON(), jwt_secret, { expiresIn: "1h" }); //{ expiresIn: '365d' }
      res.status(200).send({ ok: true, message: "Welcome back", token });
    } else return res.status(200).send({ ok: false, message: "Invalid data" });
  } catch (error) {
    res.status(400).send({ ok: false, error });
  }
};

const verify_token = (req, res) => {
  const token = req.headers.authorization;
  jwt.verify(token, jwt_secret, (err, user) => {
    err
      ? res.status(400).send({ ok: false, message: "Something went wrong" })
      : res.status(200).send({ ok: true, user });
  });
};

module.exports = { register, login, verify_token };
