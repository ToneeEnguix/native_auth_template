const port = "3040"; // 3040 should be replaced with your server port
const IP = "192.168.1.95"; // to find out your IP you can run ifconfig in the terminal and under en0 you will see two IPs, inet is the one we use

const URL = `http://${IP}:${port}`;

export { URL };

// import axios from "axios";

// const port = "3040"; // 3040 should be replaced with your server port
// const IP = "192.168.1.95"; // for production, to find out your IP, run ifconfig in terminal. In en0 there are two IPs, use inet. Once deployed use IP from deployment.

// const URL = `http://${IP}:${port}`; // we cannot use localhost as for web because mobile and computer don't share localhost. We use network IP. Both devices need to be connected to the same WiFi network.

// const customInstance = axios.create({
//   baseURL: URL,
//   headers: { Accept: "application/json" },
// });

// export default customInstance;
